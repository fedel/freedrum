# Bateria eletrônica com software livre 

Esse repositório visa reunir as informações para que você possa montar sua
própria bateria eletrônica. A ideia deste projeto é que toda a camada de
software seja livre.

# Materiais necessários

* Fios
* Piezos
* Diodos
* Transistores
* Arduino Mega
* Computador / Raspberry pi (versão em desenvolvimento)
* ...

# Passos

# Passo 1 - Hardware

Neste passo mostramos como montar a bateria fisicamente, além de como soldar as
peças necessárias para seu funcionamento.

* Seguir vídeo: https://www.youtube.com/watch?v=Gw4OXwBH4sc

# Passo 2 - Arduino

Neste passo mostramos qual código utilizar no seu arduíno e possíveis
configurações que deve definir.

Ao fim deste passo você deve ter uma bateria eletrônica que emite mensagens
MIDI.

* Código para download:

https://drive.google.com/file/d/0B8Xfi3nDnx5NUUtPSXpDZ19Dejg/view

# Passo 3 - Tocando som - Versão 1 : Computador

## Softwares necessários

* hairless-midi - http://projectgus.github.io/hairless-midiserial/
* hydrogen - http://hydrogen-music.org/

O hairless-midi pode ser substituído pela alternativa que roda via linha de
comando ttymidi - http://www.varal.org/ttymidi/

## Como fazer



# Passo 3 - Tocando som - Versão 2 : Raspberry pi

A outra alternativa é utilizar um raspberry pi. Os testes com esta versão ainda
não estão com um desempenho adequado (longo atraso). Provavelmente estes
problemas estão relacionados com o uso de um raspberry antigo e o uso da placa
de som acomplada (e não uma externa)


## Softwares necessários

* ttymidi - http://www.varal.org/ttymidi/ - Faz uma "ponte" que transforma um
  equipamento que está mandando mensagens seriais/MIDI (como um arduino) em um
  equipamento MIDI para o alsa
* jack - para ter um melhor desempenho com o sistema de som
* hydrogen - para tocar os sons da bateria
* xvfb - para executar o hydrogen sem um monitor

## Passo-a-passo

* Conectar o arduíno ao raspberry
* Subir o ttymidi 
Para isso precisará saber em qual caminho o arduíno está conectado, por padrão
ele fica em /dev/ttyACM0 ou /dev/ttyACM1 . Seguindo esse padrão o comando à ser
executado deverá ser:

`ttymidi -s /dev/ttyACM0 &`

Esse comando iniciará o serviço do ttymidi e tornará o arduíno um equipamento
disponível para conexão.

* Abrir e Hydrogen
Para esse passo é necessário um terminal gráfico. Pode-se executar o hydrogen
conectando um monitor ao raspberry pi ou então acessando o raspberry pi via
outro linux com o comando `ssh -X`, assim pode abrir a interface gráfica do
hydrogen no seu computador. 

Esse passo só precisará ser executado uma primeira vez para configurar o
hydrogen, nas próximas execuções não será necessário um terminal gráfico.

* Abrir o Jack

Configurar para conseguir a menor latência e conectar o ttymidi ao hydrogen.

* Testar!

Nesse ponto você já deve ser capaz de ouvir som saindo do raspberry quando
tocar a bateria.

# Referências

* https://www.youtube.com/watch?v=Gw4OXwBH4sc
